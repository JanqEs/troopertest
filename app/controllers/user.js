const db = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config/auth.js');
const User = db.user;
const Op = db.Sequelize.Op;

// Create and Save a new User
exports.create = async (req, res) => {
  try {
    const userName = req.body.userName;
    var condition = { username: { [Op.like]: `%${userName}%` } };
    const result = await User.findAll({ where: condition });

    if (result.length) {
      throw 'duplicate';
    }

    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);
    // Create a User
    const login = {
      username: req.body.userName,
      password: password,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      dateOfBirth: req.body.dateOfBirth,
      phoneNumber: req.body.phoneNumber,
      address: req.body.address,
    };

    // Save User in the database
    User.create(login)
      .then((data) => {
        res.json(data);
      })
      .catch((err) => {
        res.status(500).json({
          message: err.message || 'Some error occurred while creating the User.',
        });
      });
  } catch (err) {
    err === 'duplicate'
      ? res.status(500).json({
          message: 'User has been used. Please use another login',
        })
      : res.status(500).json({
          message: err.message,
        });
  }
};

exports.findAll = (req, res) => {
  const firstName = req.query.firstName;
  var condition = firstName ? { firstName: { [Op.like]: `%${firstName}%` } } : null;

  User.findAll({ where: condition })
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.status(500).json({
        message: err.message || 'Some error occurred while retrieving logins.',
      });
    });
};

// Find a single User with an login
exports.findOne = (req, res) => {
  const id = req.params.id;

  User.findByPk(id)
    .then((data) => {
      if (data) {
        res.json(data);
      } else {
        res.status(404).json({
          message: `Cannot find User with id=${id}.`,
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        message: `Error retrieving User with id= ${id}`,
      });
    });
};

exports.login = async (req, res) => {
  try {
    const userName = req.body.userName;
    var condition = { username: { [Op.like]: `%${userName}%` } };
    const result = await User.findOne({ where: condition });

    if (result === null) {
      throw 'notFound';
    }

    const isMatch = await bcrypt.compare(req.body.password, result.password);
    if (!isMatch) {
      throw 'notMatch';
    }
    const payload = {
      user: {
        id: result.id,
      },
    };
    jwt.sign(payload, config.jwtSecret, { expiresIn: 36000 }, (err, token) => {
      if (err) throw err;
      res.status(200).json({ success: true, token });
    });
  } catch (err) {
    err === 'notFound' || err === 'notMatch'
      ? res.status(500).json({
          message: 'The User or password you entered is incorrect. Please try again. ',
        })
      : res.status(500).json({
          message: err.message,
        });
  }
};
