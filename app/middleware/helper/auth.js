const jwt = require('jsonwebtoken');
const config = require('../../config/auth.js');

exports.auth = [
  (req, res, next) => {
    // Get token from header
    let token = req.header('x-auth-token');
    // Check if no token
    if (!token) {
      return res.status(401).json({ msg: 'No token, authorization denied' });
    }

    // Verify token
    try {
      const decoded = jwt.verify(token, config.jwtSecret);
      if (decoded) {
        if (decoded.user) {
          req.user = decoded.user;
        } else {
          throw 'notAuthorized';
        }
      }

      next();
    } catch (err) {
      err === 'notAuthorized'
        ? res.status(401).json({ msg: 'You are not authorized' })
        : res.status(401).json({ msg: 'Token is not valid' });
    }
  },
];
