exports.validateResource = (resourceSchema) => async (req, res, next) => {
  const resource = req.body;
  try {
    // throws an error if not valid
    await resourceSchema.validate(resource, { abortEarly: false });
    next();
  } catch (e) {
    // console.error(e);\
    const op = e.inner.map(function (item) {
      return item.path;
    });
    const errors = [];
    op.forEach((element, index) => {
      errors.push({ fieldError: element, description: e.errors[index] });
    });
    res.status(400).json({ errors: convertArrayToObject(errors, 'fieldError') });
  }
};

const convertArrayToObject = (array, key) => {
  const initialValue = {};
  return array.reduce((obj, item) => {
    return {
      ...obj,
      [item[key]]: item.description,
    };
  }, initialValue);
};
