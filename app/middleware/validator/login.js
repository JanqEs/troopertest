const yup = require('yup');

exports.loginSchema = yup.object({
  userName: yup.string().required('Username is a required field'),
  password: yup.string().required('Password is a required field'),
});
