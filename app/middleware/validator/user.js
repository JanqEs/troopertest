const yup = require('yup');

exports.userSchema = yup.object({
  userName: yup.string().required('Username is a required field'),
  password: yup
    .string()
    .min(8, 'Password must be at least 8 characters.')
    .matches(/(?=.*[A-Z])/, 'Password must have at least 1 uppercase')
    .required('Password is a required field'),
  firstName: yup.string().required('First Name is a required field'),
  lastName: yup.string().required('Last Name is a required field'),
  dateOfBirth: yup.date().required('Date Of Birth is a required field'),
  phoneNumber: yup.string().required('Phone Number is a required field'),
  address: yup.string().required('Phone Number is a required field'),
});
