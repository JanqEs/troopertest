module.exports = (app) => {
  const user = require('../controllers/user');
  const { auth } = require('../middleware/helper/auth');
  const { userSchema } = require('../middleware/validator/user');
  const { loginSchema } = require('../middleware/validator/login');

  const { validateResource } = require('../middleware/helper/validateResource');
  let router = require('express').Router();

  // Create a new Login
  router.post('/', validateResource(userSchema), user.create);

  // Retrieve all Login
  router.get('/', auth, user.findAll);

  // Retrieve a single Login with ID
  router.get('/:id', user.findOne);

  // Login user
  router.post('/login', validateResource(loginSchema), user.login);

  app.use('/api/user', router);
};
